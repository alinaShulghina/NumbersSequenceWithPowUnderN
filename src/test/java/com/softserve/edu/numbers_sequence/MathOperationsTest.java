package com.softserve.edu.numbers_sequence;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alin- on 15.12.2017.
 */
public class MathOperationsTest {

    @Test
    public void getMaxNumberWithPowUnderTest1000() {
        assertEquals(31, MathOperations.getMaxNumberWithPowUnder(1000));
    }

    @Test
    public void getMaxNumberWithPowUnderTest4() {
        assertEquals(1, MathOperations.getMaxNumberWithPowUnder(4));
    }

    @Test
    public void getMaxNumberWithPowUnderTestNegativeN() {
        assertEquals(-1, MathOperations.getMaxNumberWithPowUnder(-100));
    }

    @Test
    public void getMaxNumberWithPowUnderTestZeroN() {
        assertEquals(-1, MathOperations.getMaxNumberWithPowUnder(0));
    }

    @Test
    public void getMaxNumberWithPowUnderTest70() {
        assertEquals(8, MathOperations.getMaxNumberWithPowUnder(70));
    }

    @Test
    public void getMaxNumberWithPowUnderTest2000() {
        assertEquals(44, MathOperations.getMaxNumberWithPowUnder(2000));
    }

}