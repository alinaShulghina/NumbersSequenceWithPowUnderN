package com.softserve.edu.numbers_sequence;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alin- on 15.12.2017.
 */
public class MathOperations {
    /*
        Binary search.
        Get max number wich pow is under given param
     */
    public static int getMaxNumberWithPowUnder(int n) {
        int left = -1;
        int right = n;
        int middle;
        while (left < right - 1) {
            middle = (left + right) / 2;
            if (Math.pow(middle, 2) < n) {
                left = middle;
            } else {
                right = middle;
            }
        }
        return left;
    }
}



