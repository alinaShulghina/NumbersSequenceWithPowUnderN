package com.softserve.edu.numbers_sequence;

/**
 * Created by alin- on 11.12.2017.
 */
public class Main {

    public static void main(String[] args) {
        try {
            int n = Integer.valueOf(args[0]);
            for (int i = 0; i <= MathOperations.getMaxNumberWithPowUnder(n); i++) {
                System.out.print(i + " ");
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("No input params! Please provide at least one number!");
        } catch (NullPointerException e) {
            System.out.println("Strings are not allowed!");
        }
    }

}
